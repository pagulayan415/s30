/*
1) Create a folder called s30
2) Make a file called index.js
3) Open your terminal to s30
4) Login to MongoDB Atlas
5) inside s30 npm init
*/


// Step 7 import express
const express = require('express');

// step 13
const mongoose = require('mongoose');

// Step 8 create express app
const app = express();

// Step 9, create a const called port
const port = 3001

// Step 10 setup server to handle data from requests
	// Allows our app to read JSON data
app.use(express.json())

	// Allows our app to read data from forms
app.use(express.urlencoded({extended:true}));


// Step 11 add listen method for requests
// app.listen(port, ()=> console.log(`Server running at ${port}`));

// Step 12 install mongooose 'npm install mongoose'

// Step 13 import mongoose
	// Line 14	// 'const mongoose = require('mongoose')'

// Step 14 Go to MongoDB Atlas and change the Network access to 0.0.0.0

// Step 15 Get connection string and change the password to your password
	// mongodb+srv://admin:admin1234@zuitt-bootcamp.u57jm.mongodb.net/myFirstDatabase?retryWrites=true&w=majority

// Step 16 change myFirstDatabase to s30. MongoDB wll automatically create the database for us
	// mongodb+srv://admin:admin1234@zuitt-bootcamp.u57jm.mongodb.net/s30?retryWrites=true&w=majority

// Step 17 Connecting to MongoDB Atlas - add .connect() medthod
	// mongoose.connect();

// Step 18 
	// A. Add the connectin string
	// B. Add this object to allow connection
		/*
			{
				useNewUrlParser: true,
				useUnifiedTopology: true
			}
		*/	
mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.u57jm.mongodb.net/s30?retryWrites=true&w=majority',
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Step 19 Set notification for connection success or failure by using .connection property of mongoose
// mongoose.connection;

// Step 20 store it in a variable called db.
let db = mongoose.connection;

// Step 21
db.on("error", console.error.bind(console, "connection error"));

// Step 22 if the connection is successful, out this in the console
db.once("open", ()=> console.log("We're connected to the cloud database"))

// Step 23 Schemas the structure of the documents to be written in the database
	// Schemas act as blueprints to our data
const taskSchema = new mongoose.Schema({
	// Define the fields with the corresponding data type
	// For a task, it needs a "task name" and "task status"
	// There is a field called "name" and its data type is "String"
	name: String,
	// There is a field called "status" that is a "String" and the default value is "pending"
	status: {
		type: String,
		default: "pending"
	}
});

// Models use Schemas and they act as the middleman from the server (JS code) to our database
// Step 24 Create a model
	// Models must be in singular form and capitalized
	// The first parameter of the Mongoos model method indicates the collection
	// in where to store the data (tasks, plural form of the model)
const Task = mongoose.model("Task", taskSchema)

/*
Business Logic
1. Add a functionality to check if there are duplicate tasks
	-If the task already exist in the database, we return an error
	-If the task doesn't exist in the dataase, we add it in the database
*/

// Step 25 Create route to add task
// app.post('/tasks', (req, res)=>{

// })

// Step 26 Check if the task already exists
	//Use the Task model to interact with the tasks collection
app.post('/tasks', (req, res)=>{
	Task.findOne({name: req.body.name}, (err, result)=>{
		if(result != null && result.name == req.body.name){
			// Return a message to the client/Postman
			return res.send('Duplicate task found')
		}else{
			let newTask = new Task({
				name: req.body.name
			})
			newTask.save((saveErr, savedTask)=>{
				if(saveErr){
					return console.error(saveErr)
				}else{
					return res.status(201).send('New task created')
				}
			})
		}
	})
}) 

// Step 27 Get all tasks
	// See all the contents of the task collection via the Task model 
	// "find" is a Mongoose method that is similar to Mongodb "find", and an empty "{}" means it returns all the documents and stores them into the "result" parameter of the callback function
app.get('/tasks', (req, res)=>{
	Task.find({}, (err, result)=>{
		if(err){
			return console.log(err)
		}else{
			return res.status(200).json({data:result})
		}
	})
})

// Activity
// 1. Create a User schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

// 2. Make a Model
const User = mongoose.model("User", userSchema)

// 3. Register a User
/*
	1. Add a functionality to check if there are duplicate users
	-If the user already exist in the database, we return an error
	-If the user doesn't exist in the dataase, we add it in the database
*/ 
app.post('/signup', (req, res)=>{
	User.findOne({username: req.body.username}, (err, result)=>{
		if(result != null && req.body.password != null && result.username == req.body.username){
			return res.send('Duplicate User found')
		}else{
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})
			newUser.save((saveErr, savedTask)=>{
				if(saveErr){
					return console.console.error((saveErr));
				}else{
					return res.status(201).send('New User created')
				}
			})
		}
	})
})

app.listen(port, ()=> console.log(`Server running at ${port}`));